-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 23-Nov-2019 às 16:51
-- Versão do servidor: 10.4.6-MariaDB
-- versão do PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lp2_pro_joel`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentario`
--

CREATE TABLE `comentario` (
  `id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `comenta` text NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `comentario`
--

INSERT INTO `comentario` (`id`, `deleted`, `nome`, `email`, `comenta`, `last_modified`) VALUES
(1, 0, 'Joel', 'joel@gmail.com', 'Olá, sejam todos bem vindos a área de comentários, deixe seu elogio, seu comentário, ou sua reclamação, obrigado por tudo, tenham uma boa semana !', '2019-11-20 15:59:53'),
(2, 0, 'Bruno', 'bruno@gmail.com', 'O site ficou bem otimizado, muito bom !!!', '2019-11-20 17:42:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `login`
--

INSERT INTO `login` (`id`, `deleted`, `nome`, `email`, `senha`, `last_modified`) VALUES
(1, 0, 'Joel', 'joel@gmail.com', '123456789', '2019-11-20 16:00:26');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `valor` decimal(10,2) NOT NULL,
  `last_modified` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `deleted`, `imagem`, `descricao`, `valor`, `last_modified`) VALUES
(4, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/167681-255-255/DX23-PALHETA.jpg?v=636011602325500000', 'Palheta do Limpador Para-brisa - DX23 - Dyna - O Par     ', '69.90', '2019-11-20 17:48:38'),
(5, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/163621-255-255/parafuso-cromado-final.jpg?v=635678891896430000', 'Parafuso de Roda Cromado M12X1,50 Raio 12,5 17mm - Cada     ', '1.50', '2019-11-20 17:49:17'),
(6, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/160394-255-255/amortecedor-porta-mala-volkswagen-golf-ano-97-ate-03-marca-ammec-ap05-e6a6f9.jpg?v=635620169815800000', 'Amortecedor Porta Malas Golf ano 1997 até 2003 - AP05 - Ammec     ', '63.00', '2019-11-20 17:50:16'),
(7, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/161144-255-255/volante-jetta-rallye-similar-ao-original-volkswagen-c-cubo-252e97.jpg?v=635620170643330000', 'Volante Jetta - Rallye - Similar ao Original Volkswagen - C/Cubo     ', '358.00', '2019-11-20 17:51:20'),
(8, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/163940-255-255/embreagem-mcarm.jpg?v=635756721988600000', 'Kit de Embreagem Hyundai Tuscon Kia Sportage 2.0 - MK10058 - MecArm     ', '830.00', '2019-11-20 17:52:49'),
(9, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/163748-255-255/lampada-H4-RW372.jpg?v=635702397911500000', 'Lâmpada H4 Alta Performance 12V 60/55W P43t - Cor Azul - O Par', '80.00', '2019-11-20 17:55:31'),
(10, 0, 'http://rumoautopecas.vteximg.com.br/arquivos/ids/160817-255-255/interruptor-da-luz-de-re-mercedes-benz-classe-a-160-190-ano-99-ate-05-marca-original-a-168-545-01-14-a9ea2a.jpg?v=635620170332200000', 'Interruptor da Luz de Ré Mercedes A160 A190 ano 1999 até 2005 - Original', '147.00', '2019-11-20 17:57:12');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `comentario`
--
ALTER TABLE `comentario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
