<?php

class LibProduto {

    private $db;
    private $id;
    private $imagem;
    private $descricao;
    private $valor;

    public function __construct($descricao = null){

        $this->descricao = $descricao;

        $ci = & get_instance();
        $this->db = $ci->db;

    }

    public function getALL(){
        
        $html = '';
        
        $td = $this->db->get('produto');
        $produto = $td->result();
        
        foreach($produto AS $produto){
        
            $html .= $this->getRow($produto);
        
        }
        
        return $html;
        
    }

    public function getALL2(){
        
        $html = '';
        
        $td = $this->db->get('produto');
        $produto = $td->result();
        
        foreach($produto AS $produto){
        
            $html .= $this->getRow2($produto);
        
        }
        
        return $html;
        
    }

    public function getProduto($id){
        
        $con['id'] = $id;
        $td = $this->db->get_WHERE('produto', $con);
        
        return $td->row();
        
    }

    public function setId($id){
        
        $this->id = $id;

    }

    public function setImagem($imagem){
        
        $this->imagem = $imagem;
        
    }

    public function setDescricao($descricao){
        
        $this->descricao = $descricao;
        
    }

    public function setValor($valor){

        $this->valor = $valor;

    }

    public function grava(){
        
        $sql = "INSERT INTO produto (imagem, descricao, valor) values ('$this->imagem', '$this->descricao', '$this->valor')";
        $this->db->query($sql);
        
    }

    public function atualiza($id){
        
        $sql = "UPDATE produto SET imagem = '$this->imagem', descricao = '$this->descricao', valor = '$this->valor' WHERE id = $id";
        $this->db->query($sql);
        
    }

    public function delete($id){
        
        $this->db->delete('produto', array('id' => $id)); 
        
    }

    function getData(){
        
        $query = null;
        $sql = "SELECT * FROM produto WHERE id = ".$this->id;
        $query = $this->db->query($sql);
        return $query->row();
         
    }

    private function getRow($produto){
        
        $edit = '<a href="'.base_url('Produto/editar/'.$produto->id).'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
        $delete = '<a href="'.base_url('Produto/delete/'.$produto->id).'"> <i class="fa fa-trash-alt" aria-hidden="true"></i></i></a>';
        
        $html = 
        
        '<div class="row mt-3 wow fadeIn">
            <div class="col-lg-5 col-xl-4 mb-4">
                <div class="view overlay rounded z-depth-1">
                    <img src="'.$produto->imagem.'" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4 mt-5">
                <div class="float-right">'.$edit.$delete.'</div>
                <h3 class="mb-3 font-weight-bold dark-grey-text"><strong>R$ '.$produto->valor.'</strong></h3>
                <p class="grey-text">'.$produto->descricao.'</p>
            </div>
        </div>
        <hr class="mb-5">';
        
        return $html;
        
    }

    private function getRow2($produto){
        
        $html = 
        
            '<div class="row mt-3 wow fadeIn">
                <div class="col-lg-5 col-xl-4 mb-4">
                    <div class="view overlay rounded z-depth-1">
                        <img src="'.$produto->imagem.'" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4 mt-5">
                    <h3 class="mb-3 font-weight-bold dark-grey-text"><strong>R$ '.$produto->valor.'</strong></h3>
                    <p class="grey-text">'.$produto->descricao.'</p>
                </div>
            </div>
            <hr class="mb-5">';
        
        return $html;
        
    }

}