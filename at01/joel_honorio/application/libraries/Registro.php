<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro{

    private $db;

    public function __construct(){

        $ci = & get_instance();
        $this->db = $ci->db;

    }

    public function validate($data){

        $aux['email'] = $data['email'];
        $aux['senha'] = $data['senha'];
        $this->db->where($aux);
        return $this->db->get('login')->row();

    }

}