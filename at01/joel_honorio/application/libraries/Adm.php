<?php

class Adm{

    private $db;
    private $id;
    private $nome;
    private $email;
    private $senha;

    public function __construct($nome = null, $email = null){

        $this->nome = $nome;
        $this->email = $email;

        $ci = & get_instance();
        $this->db = $ci->db;

    }

    public function getALL(){

        $html = '';

        $td = $this->db->get('login');
        $login = $td->result();

        foreach($login AS $login){

            $html .= $this->getRow($login);

        }

        return $html;

    }

    public function getAdm($id){

        $cond['id'] = $id;
        $td = $this->db->get_WHERE('login', $cond);

        return $td->row();

    }

    public function setId($id){
        
        $this->id = $id;

    }

    public function setNome($nome){
        
        $this->nome = $nome;
        
    }

    public function setEmail($email){

        $this->email = $email;

    }

    public function setSenha($senha){

        $this->senha = $senha;

    }

    public function grava(){

        $sql = "INSERT INTO login (nome, email, senha) values ('$this->nome', '$this->email', '$this->senha')";
        $this->db->query($sql);

    }

    public function atualiza(){
        
        $sql = "UPDATE login SET nome = '$this->nome', email = '$this->email', senha = '$this->senha' WHERE id = $this->id";
        print_r($sql);
        echo "LOL";
        $this->db->query($sql);
        
    }

    public function delete($id){

        $this->db->delete('login', array('id' => $id));

    }

    function getData(){

        $query = null;
        $sql = "SELECT * FROM login WHERE id = ".$this->id;
        $query = $this->db->query($sql);
        return $query->row();

    }

    private function getRow($Adm){

        $edit = '<a href="'.base_url('Administrador/editar/'.$Adm->id).'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
        $delete = '<a href="'.base_url('Administrador/delete/'.$Adm->id).'"> <i class="fa fa-user-times" aria-hidden="true"></i></i></a>';

        $html = '
        
        <div class="card">
            <div class="card-header btn-info lighten-1 white-text">'.$Adm->nome.'<div class="float-right">'.$edit.$delete.'</div></div>
            <div class="card-body">
                <p class="card-text">E - mail: '.$Adm->email.'</p>
            </div>
        </div><br>';

        return $html;

    }
    
}