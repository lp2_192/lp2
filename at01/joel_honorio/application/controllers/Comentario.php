<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comentario extends CI_Controller {

    public function __construct(){

        parent::__construct();

        $this->load->helper('url');

        $this->load->model('InfoModel','header');
        $dados = $this->header->getHeader();
        $this->load->view('common/header', $dados);
        
    }

    public function cadastro(){
        
        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);

        $this->load->model('ComentModel', 'coment');
                
        if($this->coment->registraComentario()){
                        
            redirect('Comentario/comentario');
                        
        }
                        
        $data['action'] = 'Comentario/cadastro';
        $this->load->view('itens/form_com', $data);

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);
                
        $this->load->view('common/rodape');
                
    }

    public function comentario(){
        
        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);

        $this->load->model('ComentModel', 'coment');
        $data['content'] = $this->coment->getALL2();
        $this->load->view('itens/lista_com', $data);

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);
        
        $this->load->view('common/rodape');
                
    }

    public function exibe(){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('ComentModel', 'coment');
            $data['content'] = $this->coment->getALL();
            $this->load->view('itens/lista_com_adm', $data);
            
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');
                
    }
          
    public function editar($id = 0){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('ComentModel', 'coment');
            
            if($this->coment->editarComentario()){
            
                redirect('Comentario/exibe');
            
            }
                            
            $data['action'] = 'Comentario/editar';
            $data['comentario'] = $this->coment->getComentario($id);
            $this->load->view('itens/form_com', $data);
                        
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');
        
    }
          
    public function delete($id = 0){

        if($this->session->usuario->logado){
        
            $this->load->model('ComentModel', 'coment');
            $this->coment->deletaComentario($id);
            
            redirect('Comentario/exibe');

        }
        
        else redirect('Login/index');
        
    }

}