<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	public function index(){

        $this->load->model('LoginModel', 'login');
        $this->login->validate();

        $html = $this->load->view('itens/form_login', null, true);
        $this->show($html);

	}

	public function logout(){

        $this->session->sess_destroy();
        redirect('Controle/index');

    }

    private function show($conteudo){

        $this->load->model('InfoModel','header');
        $dados = $this->header->getHeader();
        $this->load->view('common/header', $dados);

        echo $conteudo;

        $this->load->view('common/rodape');

    }
    
}