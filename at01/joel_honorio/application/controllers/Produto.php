<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produto extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->helper('url');

        $this->load->model('InfoModel','header');
        $dados = $this->header->getHeader();
        $this->load->view('common/header', $dados);
        
    }

    public function cadastroProduto(){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
                    
            $this->load->model('ProdutoModel', 'produto');
                    
            if($this->produto->registraProduto()){

                redirect('Produto/exibe');

            }
                            
            $data['action'] = 'Produto/cadastroProduto';
            $this->load->view('itens/form_pro', $data);
        
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');
                
    }

    public function produto(){
        
        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);
        
        $this->load->model('ProdutoModel', 'produto');
        $data['content'] = $this->produto->getALL2();
        $this->load->view('itens/lista_pro', $data);

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);
        
        $this->load->view('common/rodape');
                
    }

    public function exibe(){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('ProdutoModel', 'produto');
            $data['content'] = $this->produto->getALL();
            $this->load->view('itens/lista_pro_adm', $data);
            
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');
                
    }
       
    public function editar($id = 0){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('ProdutoModel', 'produto');
            
            if($this->produto->editarProduto()){
            
                redirect('Produto/exibe');
            
            }
                            
            $data['action'] = 'Produto/editar';
            $data['produto'] = $this->produto->getProduto($id);
            $this->load->view('itens/form_pro', $data);
                        
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');
        
    }
  
    public function delete($id = 0){

        if($this->session->usuario->logado){
            
            $this->load->model('ProdutoModel', 'produto');
            $this->produto->deletaProduto($id);
            
            redirect('Produto/exibe');

        }
        
        else redirect('Login/index');
        
    }

}