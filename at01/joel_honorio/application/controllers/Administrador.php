<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administrador extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->helper('url');

        $this->load->library('session');

        $this->load->model('InfoModel','header');
        $dados = $this->header->getHeader();
        $this->load->view('common/header', $dados);
        
    }

    public function adm(){

        if($this->session->usuario->logado){

            $this->load->view('common/navbar_adm');

            $this->load->view('itens/full');
            
            $this->load->view('common/rodape');

        }
        
        else redirect('Login/index');

    }

    public function cadastro(){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
                    
            $this->load->model('AdmModel', 'adm');
                    
            if($this->adm->registraAdm()){
                            
                redirect('Administrador/adm');
                            
            }
                            
            $data['act'] = 'Administrador/cadastro';
            $this->load->view('itens/form_adm', $data);
                    
            $this->load->view('common/rodape');

        }
    
        else redirect('Login/index');
                
    }

    public function exibe(){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('AdmModel', 'login');
            $data['content'] = $this->login->getALL();
            $this->load->view('itens/lista_adm', $data);
            
            $this->load->view('common/rodape');

        }
    
        else redirect('Login/index');
                
    }

    public function editar($id = 0){

        if($this->session->usuario->logado){
        
            $this->load->view('common/navbar_adm');
            
            $this->load->model('AdmModel', 'adm');

            if($this->adm->editarAdm()){
            
                redirect('Administrador/exibe');
            
            }
                            
            $data['act'] = 'Administrador/editar';
            $data['adm'] = $this->adm->getAdm($id);
            
            $this->load->view('itens/form_adm', $data);

            $this->load->view('common/rodape');

        }
    
        else redirect('Login/index');
        
    }
    
    public function delete($id = 0){

        if($this->session->usuario->logado){
        
            $this->load->model('AdmModel', 'login');
            $this->login->deletaAdm($id);
            
            redirect('Administrador/exibe');

        }
    
        else redirect('Login/index');
     
    }

}