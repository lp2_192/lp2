<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controle extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->helper('url');

        $this->load->model('InfoModel','header');
        $dados = $this->header->getHeader();
        $this->load->view('common/header', $dados);

    }

    public function index(){

        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);

        $this->load->view('itens/galeria');

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);

        $this->load->view('common/rodape');

    }

    public function produto(){

        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);

        $this->load->view('itens/produto');

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);

        $this->load->view('common/rodape');

    }

    public function comentarios(){

        $this->load->model('InfoModel','navbar');
        $dados = $this->navbar->getNavbar();
        $this->load->view('common/navbar', $dados);

        $this->load->view('itens/comentarios');

        $this->load->model('InfoModel','footer');
        $dados = $this->footer->getFooter();
        $this->load->view('common/footer', $dados);

        $this->load->view('common/rodape');

    }

    public function admin(){

        $this->load->model('InfoModel','admin');
        $dados = $this->admin->getAdmin();
        $this->load->view('itens/admin', $dados);

        $this->load->view('common/rodape');

    }

}