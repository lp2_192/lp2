<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InfoModel extends CI_Model{

    function getHeader(){

        $header['titulo'] = "FULLDECK";

        return $header;
    }

    function getNavbar(){

        $navbar['link1'] = "FULLDECK";
        $navbar['link2'] = "Início";
        $navbar['link3'] = "Peças";
        $navbar['link4'] = "Comentários";

        return $navbar;
    }

    function getFooter(){

        $footer['ano'] = "2019";
        $footer['empresa'] = "fulldeck.com";

        return $footer;

    }

    function getAdmin(){

        $admin['titulo'] = "Login Administrador";
        $admin['email'] = "E - mail";
        $admin['senha'] = "Senha";
        $admin['alerta'] = "*Essa área é restrita ao uso exclusivo do administrador do sistema, não é permitido o acesso de terceiros.";
        $admin['botao'] = "Entrar";

        return $admin;

    }

}