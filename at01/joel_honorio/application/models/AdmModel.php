<?php
include APPPATH .'libraries/Adm.php';

class AdmModel extends CI_Model{

    public function registraAdm(){
        
        if(sizeof($_POST) == 0) return false;

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('nome', 'Nome', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[50]');
        $this->form_validation->set_rules('senha', 'senha', 'required|min_length[8]');
                
        if($this->form_validation->run()){
        
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $senha = $this->input->post('senha');
        
        $adm = new Adm();
        $adm->setNome($nome);
        $adm->setEmail($email);
        $adm->setSenha($senha);
        $adm->grava();
        
        return true;

        }
        
        else{

            return false;
            
        }
        
    }

    public function editarAdm(){
        
        if(sizeof($_POST) == 0) return false;
        $id = $this->input->post('id');
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $senha= $this->input->post('senha');
                
        $adm = new Adm();
        $adm->setNome($nome);
        $adm->setEmail($email);
        $adm->setSenha($senha);
        $adm->setId($id);
        $adm->atualiza($id);
        return true;
        
    }

    public function getALL(){

        $adm = new Adm();
        return $adm->getALL();

    }

    public function getAdm($id){

        $adm = new Adm();
        return $adm->getAdm($id);

    }

    public function deletaAdm($id){

        $adm = new Adm();
        return $adm->delete($id);

    }

}