<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include APPPATH . 'libraries/Registro.php';

class LoginModel extends CI_Model{

    public function validate(){

        if(sizeof($_POST) == 0) return;
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('senha', 'Senha', 'required|min_length[8]');
        
        if($this->form_validation->run()){

            $data = $this->input->post();

            $login = new Registro();
            $user = $login->validate($data);

            if($user != null){

                $user->logado = true;
                $this->session->set_userdata('usuario', $user);

                redirect ('Administrador/adm');

            }

            else{

                echo '<center>Sua senha ou seu email estão incorretos, ou você não é um administrador.</center>';

            }

        }

    }

}