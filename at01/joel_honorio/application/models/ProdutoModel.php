<?php 
include APPPATH .'libraries/Libproduto.php';

class ProdutoModel extends CI_Model{

    public function registraProduto(){
        
        if(sizeof($_POST) == 0) return false;
        
        $this->load->library('form_validation');
                
        $this->form_validation->set_rules('imagem', 'imagem', 'required|max_length[200]');
        $this->form_validation->set_rules('descricao', 'Descricao', 'required|max_length[200]');
        $this->form_validation->set_rules('valor', 'Valor', 'required|max_length[20]');
                        
        if($this->form_validation->run()){
        
        $imagem = $this->input->post('imagem');
        $descricao = $this->input->post('descricao');
        $valor = $this->input->post('valor');
        
        $produto = new LibProduto();
        $produto->setImagem($imagem);
        $produto->setDescricao($descricao);
        $produto->setValor($valor);
        $produto->grava();
        
        return true;
        
        }
    
        else{

            return false;
            
        }
        
    }

    public function editarProduto(){
        
        if(sizeof($_POST) == 0) return false;
        
            $id = $this->input->post('id');
            $imagem = $this->input->post('imagem');
            $descricao = $this->input->post('descricao');
            $valor = $this->input->post('valor');
                
            $produto = new LibProduto();
            $produto->setImagem($imagem);
            $produto->setDescricao($descricao);
            $produto->setValor($valor);
            $produto->atualiza($id);
                
        return true;
        
    }

    public function getALL(){
        
        $produto = new LibProduto();
        return $produto->getALL();
        
    }

    public function getALL2(){
        
        $produto = new LibProduto();
        return $produto->getALL2();
        
    }

    public function getProduto($id){
        
        $produto = new LibProduto();
        return $produto->getProduto($id);
        
    }
        
    public function deletaProduto($id){
        
        $produto = new LibProduto();
        return $produto->delete($id);
        
    }

}