<?php
include APPPATH .'libraries/Coment.php';

class ComentModel extends CI_Model{

    public function registraComentario(){

        if(sizeof($_POST) == 0) return false;
        
        $this->load->library('form_validation');
                
        $this->form_validation->set_rules('nome', 'Nome', 'required|max_length[50]');
        $this->form_validation->set_rules('email', 'Email', 'required|max_length[50]');
        $this->form_validation->set_rules('comenta', 'Comentário', 'required|max_length[200]');
                        
        if($this->form_validation->run()){

        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $comenta = $this->input->post('comenta');

        $comentario = new Coment();
        $comentario->setNome($nome);
        $comentario->setEmail($email);
        $comentario->setComenta($comenta);
        $comentario->grava();

        return true;

        }

        else{

            return false;
            
        }
    
    }

    public function editarComentario(){

        if(sizeof($_POST) == 0) return false;

        $id = $this->input->post('id');
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $comenta = $this->input->post('comenta');
        
        $comentario = new Coment();
        $comentario->setNome($nome);
        $comentario->setEmail($email);
        $comentario->setComenta($comenta);
        $comentario->atualiza($id);
        
        return true;

    }

    public function getALL(){

        $comentario = new Coment();
        return $comentario->getALL();

    }

    public function getALL2(){
        
        $comentario = new Coment();
        return $comentario->getALL2();
        
    }

    public function getComentario($id){

        $comentario = new Coment();
        return $comentario->getComentario($id);

    }

    public function deletaComentario($id){

        $comentario = new Coment();
        return $comentario->delete($id);

    }

}