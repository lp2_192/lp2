<header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">

        <div class="container">

            <a class="navbar-brand waves-effect" href="http://localhost/lp2/at01/joel_honorio/Controle/index"><strong class="blue-text"><?= $link1 ?></strong></a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="navbar-nav mr-auto">

                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="http://localhost/lp2/at01/joel_honorio/Controle/index"><?= $link2 ?></a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="http://localhost/lp2/at01/joel_honorio/Produto/produto"><?= $link3 ?></a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link waves-effect" href="http://localhost/lp2/at01/joel_honorio/Comentario/comentario"><?= $link4 ?></a>
                    </li>

                </ul>

            </div>

        </div>

    </nav>

</header>