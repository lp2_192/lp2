<main class="mt-5 pt-5">

    <div class="container">

        <form class="border border-light p-5" method="POST" action="<?= base_url($action) ?>">

            <p class="h4 mb-4 text-center">Deixe seu comentário</p>

            <?php 
                $this->load->library('form_validation');
                echo validation_errors();
            ?>

            <input type="text" class="form-control mb-4" placeholder="Nome" value="<?= isset($comentario->nome) ? $comentario->nome : '' ?>"  id="nome" name="nome">
            <input type="email" class="form-control mb-4" placeholder="E-mail" value="<?= isset($comentario->email) ? $comentario->email : '' ?>" id="email" name="email">
            <input type="text" class="form-control mb-4" placeholder="Comentário" value="<?= isset($comentario->comenta) ? $comentario->comenta : '' ?>"  id="comenta" name="comenta">
            <input type="hidden" name="id" value="<?= isset($comentario->id) ? $comentario->id : '' ?>" name="id_comentario">

            <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>

        </form>

    </div>

</main>