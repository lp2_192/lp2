<main class="mt-5 pt-5">

    <div class="container">

        <header class="border border-light p-5">

            <p class="h4 mb-4 text-center">Comentários</p>

            <div class="container mt-5"><?= $content ?></div>

            <div class="text-center">
                <a href="http://localhost/lp2/at01/joel_honorio/Comentario/cadastro">
                    <button class="btn btn-info">Deixe Seu Comentário</button>
                </a>
            </div>

        </header>

    </div>

</main>