<main class="mt-5 pt-5">

    <div class="container">

        <form class="border border-light p-5" method="POST" action="<?= base_url($action) ?>">

            <p class="h4 mb-4 text-center">Cadastro de Produtos</p>

            <center>
                <h6>*Essa área é restrita ao uso exclusivo do administrador do sistema, não é permitido o acesso de terceiros.</h6>
            </center>

            <?php 
                $this->load->library('form_validation');
                echo validation_errors();
            ?>

            <input type="text" class="form-control mb-4" placeholder="Link da Imagem" value="<?= isset($produto->imagem) ? $produto->imagem : '' ?>" id="imagem" name="imagem">
            <input type="text" class="form-control mb-4" placeholder="Descricao" value="<?= isset($produto->descricao) ? $produto->descricao : '' ?>" name="descricao" id="descricao">
            <input type="text" class="form-control mb-4" placeholder="Preço" value="<?= isset($produto->valor) ? $produto->valor : '' ?>" name="valor" id="valor">
            
            <input type="hidden" name="id" value="<?= isset($produto->id) ? $produto->id : '' ?>" name="id_produto">

            <button class="btn btn-info btn-block my-4" type="submit">Cadastrar</button>

        </form>

    </div>

</main>