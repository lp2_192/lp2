<main class="mt-5 pt-5">

    <div class="container">

        <header class="border border-light p-5">

            <p class="h4 mb-4 text-center">Peças</p>

            <div class="row products"><?= $content ?></div>

        </header>

    </div>

</main>