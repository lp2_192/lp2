<main class="mt-5 pt-5">

    <div class="container">

        <form class="border border-light p-5" method="POST" action="<?= base_url($act) ?>">

            <p class="h4 mb-4 text-center">Cadastro de Administradores</p>

            <center>
                <h6>*Essa área é restrita ao uso exclusivo do administrador do sistema, não é permitido o acesso de terceiros.</h6>
            </center>

            <?php 
                $this->load->library('form_validation');
                echo validation_errors();
            ?>

            <input type="text" class="form-control mb-4" placeholder="Nome" value="<?= isset($adm->nome) ? $adm->nome : '' ?>"  id="nome" name="nome">
            <input type="email" class="form-control mb-4" placeholder="E-mail" value="<?= isset($adm->email) ? $adm->email : '' ?>" id="email" name="email">
            <input type="password" class="form-control mb-4" placeholder="Password" value="<?= isset($adm->senha) ? $adm->senha : '' ?>" name="senha" id="senha">
            <input type="hidden" name="id" value="<?= isset($adm->id) ? $adm->id : '' ?>" name="id_adm"><br>

            <button class="btn btn-info btn-block my-4" type="submit">Cadastrar</button>

        </form>

    </div>

</main>