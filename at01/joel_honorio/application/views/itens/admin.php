<main class="mt-5 pt-5">

    <div class="container mt-3">

        <?= validation_errors('<p class="alert alert-danger">', '</p>') ?>

        <div class="row">

            <form method="POST" class="col-md-6 mx-auto" action="<?= base_url('login') ?>">

                <p class="h2 text-center mb-4 mt-5"><?= $titulo ?></p>

                <div class="md-form">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <input type="text" id="email" name="email" class="form-control">
                    <label for="email"><?= $email ?></label>
                </div>

                <div class="md-form">
                    <i class="fa fa-lock prefix grey-text"></i>
                    <input type="password" id="senha"  name="senha" class="form-control">
                    <label for="senha"><?= $senha ?></label>
                </div><br><br><br>

                <center>
                    <h6><?= $alerta ?></h6>
                </center>

                <div class="text-center">
                    <button class="btn btn-deep-orange"><?= $botao ?></button>
                </div>

            </form>

        </div>

    </div>

</main>