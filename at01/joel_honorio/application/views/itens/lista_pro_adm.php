<main class="mt-5 pt-5">

    <div class="container">

        <header class="border border-light p-5">

            <p class="h4 mb-4 text-center">Peças</p>

            <center>
	            <h6>*Essa área é restrita ao uso exclusivo do administrador do sistema, não é permitido o acesso de terceiros.</h6>
            </center>

            <div class="row products"><?= $content ?></div>

        </header>

    </div>

</main>