<div class="view full-page-intro">
    <video class="video-intro" autoplay loop muted>
        <source src="https://mdbootstrap.com/img/video/animation-intro.mp4" type="video/mp4"/>
    </video>
    <div class="mask rgba-blue-light d-flex justify-content-center align-items-center">
        <div class="container">
            <center>
                <h6>*Essa área é restrita ao uso exclusivo do administrador do sistema, não é permitido o acesso de terceiros.</h6>
            </center><br>
            <div class="row d-flex h-100 justify-content-center align-items-center wow fadeIn">
                <div class="col-md-8 mb-4 white-text text-center">
                    <a href="http://localhost/lp2/at01/joel_honorio/Login/logout" class="btn btn-outline-white">Sair</a><br>
                    <a href="http://localhost/lp2/at01/joel_honorio/Administrador/cadastro" class="btn btn-outline-white">Cadastro de Adm's</a><br>
                    <a href="http://localhost/lp2/at01/joel_honorio/Produto/cadastroProduto" class="btn btn-outline-white">Cadastro de Produtos</a><br>
                    <a href="http://localhost/lp2/at01/joel_honorio/Comentario/cadastro" class="btn btn-outline-white">Cadastro de Comentários</a>
                </div>
            </div>
        </div>
    </div>
</div>