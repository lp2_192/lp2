<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controle extends CI_Controller{

    public function __construct(){

        parent::__construct();

        $this->load->helper('url');

        $this->load->view('common/header');

        $this->load->view('common/navbar');

    }

    public function index(){

        $this->load->view('itens/principal');

        $this->load->view('common/rodape');

    }

    public function button(){

        $this->load->view('itens/button');

        $this->load->view('common/rodape');

    }

    public function card(){

        $this->load->view('itens/card');

        $this->load->view('common/rodape');

    }

    public function panel(){

        $this->load->view('itens/panel');

        $this->load->view('common/rodape');

    }

}