<nav class="navbar navbar-expand-lg navbar-dark primary-color">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="http://localhost/lp2/at02/joel_honorio/Comentario/cadastro">at02</a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="http://localhost/lp2/at02/joel_honorio/Comentario/cadastro">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>

      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Html</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="http://localhost/lp2/at02/joel_honorio/Controle/card">Cards</a>
          <a class="dropdown-item" href="http://localhost/lp2/at02/joel_honorio/Controle/panel">Panel</a>
          <a class="dropdown-item" href="http://localhost/lp2/at02/joel_honorio/Controle/button">Button</a>
          <a class="dropdown-item" href="http://localhost/lp2/at02/joel_honorio/Teste/testeNumero">Teste numérico</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test/HelloTest">Testes Unitários</a>
      </li>

    </ul>
    <!-- Links -->

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->