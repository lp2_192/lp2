<main class="mt-5 pt-5">

    <div class="container">

        <form class="border border-light p-5" method="POST" action="<?= base_url($action)?>">

            <p class="h4 mb-4 text-center">Teste Numérico</p>

            <?php 
                $this->load->library('form_validation');
                echo validation_errors();
            ?>

            <input type="text" class="form-control mb-4" placeholder="Valor lançado" value="<?= isset($teste_case->vlv) ? $teste_case->vlv : '' ?>"  id="vlv" name="vlv">
            <input type="text" class="form-control mb-4" placeholder="Valor esperado" value="<?= isset($teste_case->vle) ? $teste_case->vle : '' ?>" id="vle" name="vle">            
            <input type="hidden" name="id" value="<?= isset($teste_case->id) ? $teste_case->id : '' ?>" name="id_card">
            <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>

        </form>

    </div>

</main>