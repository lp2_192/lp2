<center class="mt-5"><h2>Button</h2></center>

<main class="pt-5">
    <div class="container">
        <header class="border border-light p-5">

        <p>
            <button type="button" class="btn">Basic</button>
            <button type="button" class="btn btn-default">Default</button>
            <button type="button" class="btn btn-primary">Primary</button>
            <button type="button" class="btn btn-success">Success</button>
            <button type="button" class="btn btn-info">Info</button>
            <button type="button" class="btn btn-warning">Warning</button>
            <button type="button" class="btn btn-danger">Danger</button>
            <button type="button" class="btn btn-link">Link</button>
        </p>

        </header>
    </div>
</main>

<main class="pt-5">
    <div class="container">
        <header class="border border-light p-5">

        <div class="w3-code notranslate htmlHigh">
        
            <span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Basic<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-default"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Default<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-primary"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Primary<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-success"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Success<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-info"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Info<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-warning"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Warning<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-danger"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Danger<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span><br><span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>button<span class="attributecolor" style="color:red"> type<span class="attributevaluecolor" style="color:mediumblue">="button"</span> class<span class="attributevaluecolor" style="color:mediumblue">="btn btn-link"</span></span><span class="tagcolor" style="color:mediumblue">&gt;</span></span>Link<span class="tagnamecolor" style="color:brown"><span class="tagcolor" style="color:mediumblue">&lt;</span>/button<span class="tagcolor" style="color:mediumblue">&gt;</span></span> </div>
  
        </header>
    </div>
</main><br>