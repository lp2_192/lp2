<main class="mt-5 pt-5">

    <div class="container">

        <form class="border border-light p-5" method="POST" action="<?= base_url($action)?>">

            <p class="h4 mb-4 text-center">Cadastro de Cards na Atividade</p>

            <?php 
                $this->load->library('form_validation');
                echo validation_errors();
            ?>

            <input type="text" class="form-control mb-4" placeholder="Título do Card" value="<?= isset($card->titulo) ? $card->titulo : '' ?>"  id="titulo" name="titulo">
            <input type="text" class="form-control mb-4" placeholder="Descrição do Card" value="<?= isset($card->texto) ? $card->texto : '' ?>" id="texto" name="texto">            
            <input type="hidden" name="id" value="<?= isset($card->id) ? $card->id : '' ?>" name="id_card">
            <button class="btn btn-info btn-block my-4" type="submit">Enviar</button>

        </form>

    </div>

</main>