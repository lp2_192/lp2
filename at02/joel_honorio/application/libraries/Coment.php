<?php

class Coment {

    private $db;
    private $id;
    private $titulo;
    private $texto;

    public function __construct($titulo = null, $texto = null){

        $this->titulo = $titulo;
        $this->texto = $texto;
		
        $ci = & get_instance();		
        $this->db = $ci->db;

    }

    public function getALL(){

        $html = '';

        $td = $this->db->get('card');
        $comentario = $td->result();

        foreach($comentario AS $comentario){

            $html .= $this->getRow($comentario);

        }

        return $html;

    }

    public function getALL2(){
        
        $html = '';
        
        $td = $this->db->get('card');
        $comentario = $td->result();
        
        foreach($comentario AS $comentario){
        
            $html .= $this->getRow2($comentario);
        
        }
        
        return $html;
        
    }

    public function getComentario($id){

        $cond['id'] = $id;
        $td = $this->db->get_WHERE('card', $cond);

        return $td->row();

    }

    public function setId($id){
        
        $this->id = $id;

    }

    public function setTexto($texto){
        
        $this->texto = $texto;
        
    }

    public function setTitulo($titulo){

        $this->titulo = $titulo;

    }

    public function grava(){	
        $sql = "INSERT INTO card (titulo, texto) values ('$this->titulo', '$this->texto')";        
		return ($this->db->query($sql));
    }

    public function atualiza($id){

        $sql = "UPDATE comentario SET nome = '$this->nome', titulo = '$this->titulo' WHERE id = $id";
        $this->db->query($sql);

    }

    public function delete($id){

        $this->db->delete('card', array('id' => $id));

    }

    function getData(){
        
        $query = null;
        $sql = "SELECT * FROM comentario WHERE id = ".$this->id;
        $query = $this->db->query($sql);
        return $query->row();
         
    }

    private function getRow($comentario){

        $edit = '<a href="'.base_url('Comentario/editar/'.$comentario->id).'"><i class="fa fa-edit" aria-hidden="true"></i></a>';
        $delete = '<a href="'.base_url('Comentario/delete/'.$comentario->id).'"> <i class="fa fa-times-circle" aria-hidden="true"></i></i></a>';

        $html = '

            <div class="card">
                <div class="card-header btn-info lighten-1 white-text">'.$comentario->nome.'<div class="float-right">'.$edit.$delete.'</div></div>
                <div class="card-body">
                    <p class="card-text">E - mail: '.$comentario->titulo.'</p>
                    <p class="card-text">Comentário: '.$comentario->texto.'</p>
                </div>
            </div><br>';

        return $html;

    }

    private function getRow2($comentario){

        $html = '
        
            <div class="card">
                <div class="card-header btn-info lighten-1 white-text">'.$comentario->nome.'</div>
                <div class="card-body">
                    <p class="card-text">E - mail: '.$comentario->titulo.'</p>
                    <p class="card-text">Comentário: '.$comentario->texto.'</p>
                </div>
            </div><br>';
        
        return $html;
        
    }
    
}