<?php
include APPPATH .'libraries/Coment.php';

class ComentModel extends CI_Model{

    public function registraComentario(){

        if(sizeof($_POST) == 0) return false;
        
        $this->load->library('form_validation');
                
        $this->form_validation->set_rules('titulo', 'Titulo', 'required|max_length[50]');
        $this->form_validation->set_rules('texto', 'Texto', 'required|max_length[50]');        
                        
        if($this->form_validation->run()){
        $titulo = $this->input->post('titulo');
        $texto = $this->input->post('texto');        

        $comentario = new Coment();
        $comentario->setTitulo($titulo);
        $comentario->setTexto($texto);        
        $return = $comentario->grava();
		print_r($return);	

        return true;

        }

        else{

            return false;
            
        }
    
    }

    public function editarComentario(){

        if(sizeof($_POST) == 0) return false;

        $id = $this->input->post('id');
        $nome = $this->input->post('nome');
        $email = $this->input->post('email');
        $comenta = $this->input->post('comenta');
        
        $comentario = new Coment();
        $comentario->setNome($nome);
        $comentario->setEmail($email);
        $comentario->setComenta($comenta);
        $comentario->atualiza($id);
        
        return true;

    }

    public function getALL(){

        $comentario = new Coment();
        return $comentario->getALL();

    }

    public function getALL2(){
        
        $comentario = new Coment();
        return $comentario->getALL2();
        
    }

    public function getComentario($id){

        $comentario = new Coment();
        return $comentario->getComentario($id);

    }

    public function deletaComentario($id){

        $comentario = new Coment();
        return $comentario->delete($id);

    }

}